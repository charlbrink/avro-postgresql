package com.example.avropostgresql;

import com.example.avropostgresql.domain.PersonEntity;
import com.example.avropostgresql.domain.PersonRepository;
import com.example.schema.avropostgresql.Person;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class AvroPostgresqlApplicationTests {

	public static final String NAME = "John Wick";
	public static final int AGE = 30;
	@Autowired
	PersonRepository personRepository;

	@Test
	public void contextLoads() {
		Person avroPerson = Person.newBuilder().setName(NAME).setAge(AGE).build();

		PersonEntity person = new PersonEntity();
		person.setPerson(avroPerson);
		PersonEntity persistedPerson = personRepository.save(person);

        Optional<PersonEntity> persisted = personRepository.findById(persistedPerson.getId());
        assertTrue("expected person to be retrieved", persisted.isPresent());
		assertEquals("expected person name to match", NAME, persisted.get().getPerson().getName());
		assertEquals("expected person age to match", AGE, persisted.get().getPerson().getAge().intValue());
    }

}
