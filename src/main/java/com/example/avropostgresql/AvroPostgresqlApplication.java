package com.example.avropostgresql;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvroPostgresqlApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvroPostgresqlApplication.class, args);
    }
}
