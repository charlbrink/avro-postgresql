package com.example.avropostgresql.domain;

import com.example.schema.avropostgresql.Person;
import org.apache.avro.Schema;
import org.apache.avro.generic.GenericDatumReader;
import org.apache.avro.generic.GenericDatumWriter;
import org.apache.avro.generic.GenericRecord;
import org.apache.avro.io.Decoder;
import org.apache.avro.io.DecoderFactory;
import org.apache.avro.io.Encoder;
import org.apache.avro.io.EncoderFactory;
import org.apache.avro.io.JsonEncoder;
import org.apache.avro.reflect.ReflectData;
import org.apache.avro.specific.SpecificDatumReader;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.UserType;
import org.postgresql.util.PGobject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;

public class JsonPersonType implements UserType {
    @Override
    public int[] sqlTypes() {
        return new int[]{Types.JAVA_OBJECT};
    }

    @Override
    public Class<Person> returnedClass() {
        return Person.class;
    }

    @Override
    public Object nullSafeGet(final ResultSet rs, final String[] names, final SharedSessionContractImplementor sharedSessionContractImplementor, final Object o) throws HibernateException,
            SQLException {
        if (!(rs.getObject(names[0]) instanceof PGobject)) {
            return null;
        }
        PGobject cellContent = (PGobject) rs.getObject(names[0]);
        if (cellContent.getValue() == null) {
            return null;
        }

        // TODO This code can be done better when https://issues.apache.org/jira/browse/AVRO-1443 is solved
        try (ByteArrayInputStream bais = new ByteArrayInputStream(cellContent.getValue().getBytes("UTF-8")); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Schema s = ReflectData.get().getSchema(returnedClass());
            GenericDatumReader reader = new GenericDatumReader(s);
            GenericRecord after = (GenericRecord) reader.read(null, DecoderFactory.get().jsonDecoder(s, bais));

            GenericDatumWriter<GenericRecord> writer = new GenericDatumWriter<>(Person.getClassSchema());
            Encoder binaryEncoder = EncoderFactory.get().binaryEncoder(out, null);
            writer.write(after, binaryEncoder);
            binaryEncoder.flush();

            byte[] avroData = out.toByteArray();

            SpecificDatumReader<?> specificReader = new SpecificDatumReader<>(Person.class);
            Decoder decoder = DecoderFactory.get().binaryDecoder(avroData, null);
            return specificReader.read(null, decoder);
        } catch (final Exception ex) {
            throw new RuntimeException("Failed to convert String to Object: " + ex.getMessage(), ex);
        }
    }

    @Override
    public void nullSafeSet(final PreparedStatement ps, final Object value, final int idx, final SharedSessionContractImplementor sharedSessionContractImplementor) throws HibernateException,
            SQLException {
        if (value == null) {
            ps.setNull(idx, Types.OTHER);
            return;
        }
        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            Schema s = ReflectData.get().getSchema(value.getClass());
            GenericDatumWriter writer = new GenericDatumWriter(s);
            JsonEncoder jsonEncoder = EncoderFactory.get().jsonEncoder(s, baos);
            writer.write(value, jsonEncoder);
            jsonEncoder.flush();
            ps.setObject(idx, new String(baos.toByteArray()), Types.OTHER);
        } catch (final Exception ex) {
            throw new RuntimeException("Failed to convert Object to String: " + ex.getMessage(), ex);
        }
    }

    @Override
    public Object deepCopy(final Object value) throws HibernateException {
        try {
            // use serialization to create a deep copy
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream oos = new ObjectOutputStream(bos);
            oos.writeObject(value);
            oos.flush();
            oos.close();
            bos.close();

            ByteArrayInputStream bais = new ByteArrayInputStream(bos.toByteArray());
            return new ObjectInputStream(bais).readObject();
        } catch (ClassNotFoundException | IOException ex) {
            throw new HibernateException(ex);
        }
    }

    @Override
    public boolean isMutable() {
        return true;
    }

    @Override
    public Serializable disassemble(final Object value) throws HibernateException {
        return (Serializable) this.deepCopy(value);
    }

    @Override
    public Object assemble(final Serializable cached, final Object owner) throws HibernateException {
        return this.deepCopy(cached);
    }

    @Override
    public Object replace(final Object original, final Object target, final Object owner) throws HibernateException {
        return this.deepCopy(original);
    }

    @Override
    public boolean equals(final Object obj1, final Object obj2) throws HibernateException {
        if (obj1 == null) {
            return obj2 == null;
        }
        return obj1.equals(obj2);
    }

    @Override
    public int hashCode(final Object obj) throws HibernateException {
        return obj.hashCode();
    }
}
