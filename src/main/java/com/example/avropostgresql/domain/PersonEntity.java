package com.example.avropostgresql.domain;

import com.example.schema.avropostgresql.Person;
import lombok.Data;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@TypeDef(name = "JsonPersonType", typeClass = JsonPersonType.class)
public class PersonEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    @Type(type = "JsonPersonType")
    private Person person;
}
